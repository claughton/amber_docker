#!/usr/bin/env bash
# A wrapper script to run Amber (pmemd)  via Docker
PROGNAME="$(basename $0)"
VERSION="v0.0.1"
# Helper functions for guards
error(){
  error_code=$1
  echo "ERROR: $2" >&2
  echo "($PROGNAME wrapper version: $VERSION, error code: $error_code )" >&2
  exit $1
}
check_cmd_in_path(){
  cmd=$1
  which $cmd > /dev/null 2>&1 || error 1 "$cmd not found!"
}

# Guards (checks for dependencies)
check_cmd_in_path docker
docker run -it --rm -v "$PWD":/wd -w /wd --entrypoint bash claughton/amber16 "$@"
