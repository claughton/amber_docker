Dockerized versions of Amber 16 and 18.
=================================

This repo provides Amber16/18 and Ambertools16/18 in a dockerized way. Instead of 
installing the software, you install "shims" with the same names as the key 
Amber executables that, when run, download (if neccessary) and then launch 
a Docker container to actually execute the command.
Amber and AmberTools-based shims are provided separately because of the Amber license
restrictions. 

For full instructions, please switch to the relevant branch:

- v16: Amber and AmberTools, version 16
- v16-tools; AmberTools only, version 16
- v18: Amber and AmberTools, version 18
- v18-tools: AmberTools only, version 18
    
